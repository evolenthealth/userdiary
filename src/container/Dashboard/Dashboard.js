import React, { useState, useEffect, useContext, forwardRef } from 'react'
import {
	TextField,
	Dialog,
	DialogTitle,
	Button,
	FormControl,
	FormLabel,
	FormControlLabel,
	FormHelperText,
	Grid,
	makeStyles,
	Radio,
	RadioGroup,
	Slide,
	Switch,
	Tooltip,
	Typography
} from '@material-ui/core'
import {
	KeyboardDatePicker,
	MuiPickersUtilsProvider
} from '@material-ui/pickers'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import '../../assets/CSS/style.css'
import i18next from 'i18next'
import DateFnsUtils from '@date-io/date-fns'
import { v4 as uuidv4 } from 'uuid'
import clsx from 'clsx'
import { CopyToClipboard } from 'react-copy-to-clipboard'
import { ToastContext } from '../../App'
import Loader from '../../components/Loader'
import BasicDialog from '../../components/BasicDialog'
import MessageSnackbar from '../../components/MessageSnackbar'
import SearchBuilder from '../../components/SearchBuilder'
import TableBuilder from '../../components/TableBuilder'
import NoResultFound from '../../components/NoResultFound'
import {
	TOAST_VARIANT,
	helperValidation,
	UtilityConfig,
	customIntRegx,
	customStrRegx,
	emailRegex,
	CurrentDate,
	tableSchema,
	pageList
} from '../../utility/constants'
import {
	dateFormatter,
	searchOnData,
	getFilterSortedData
} from '../../utility/functions'
import { useSettings } from '../../utility/customSettings'
import {
	getServiceAPI,
	postServiceAPI,
	deleteServiceAPI
} from '../../services/apiEndpoint'

const useStyles = makeStyles(theme => ({
	root: {
		display: 'flex'
	},
	media: {
		height: 140
	},
	logo: {
		height: '100%',
		backgroundSize: '100%'
	}
}))

const Transition = forwardRef(function Transition(props, ref) {
	return <Slide direction='up' ref={ref} {...props} />
})

const Dashboard = props => {
	// declaring all states
	const [blankObject, setBlankObject] = useState({
		id: uuidv4(),
		firstname: '',
		lastname: '',
		emailid: '',
		phone: '',
		gender: 'Male',
		dob: dateFormatter(CurrentDate, 'YYYY-MM-DD'),
		active: true
	})
	const [basicDialog, setBasicDialog] = useState({
		show: false,
		msg: '',
		title: '',
		cancel: ''
	})
	const [helper, setHelper] = useState(helperValidation.dashboard)
	const [userLevel, setUserLevel] = useState({ edit: false })
	const [dialogOpen, setDialogOpen] = useState(false)
	const [openDatePicker, setOpenDatePicker] = useState(false)
	const [selectedUser, setSelectedUser] = useState({})
	const [loader, setLoader] = useState(false)
	const [tableData, setTableData] = useState([])
	const [tableAllData, setTableAllData] = useState([])
	const [tableLength, setTableLength] = useState(0)
	const [tableProperties, setTableProperties] = useState({
		order: 'asc',
		orderBy: '',
		rowsPerPage: 5,
		activePage: 0,
		searchText: ''
	})
	const [columns, setcolumns] = useState(tableSchema)
	const { showToast } = useContext(ToastContext)
	// settings hook imported from customSetting
	// this acts as a central obj accessible across the project
	const { masterObj, setMasterObj } = useSettings()
	const classes = useStyles()

	useEffect(() => {
		setTableAllData(masterObj)
	}, [masterObj])

	useEffect(() => {
		let sData = searchOnData(tableProperties.searchText, columns, tableAllData)
		let fData = getFilterSortedData(sData, tableProperties)
		setTableData(fData)
		setTableLength(sData.length)
	}, [tableAllData, tableProperties])

	//get user api call
	const getUserData = async () => {
		setLoader(true)
		const apiValue = await getServiceAPI('getuserdata', {
			// any key: value pair in object format
			// headers
		})

		if (apiValue && Object.entries(apiValue).length > 0) {
			const infoData = apiValue.data
			const infoStatus = apiValue.statusCode
			if (infoData.hasOwnProperty('success')) {
			} else
				showToast({
					open: true,
					variant: TOAST_VARIANT.ERROR,
					message: infoData.message
				})
			setLoader(false)
		}
	}

	// put/post i.e edit/add user info
	const putpostUserData = async () => {
		setLoader(true)
		const msg = userLevel.edit
			? i18next.t('dashboard.editAction.SUCCESS')
			: i18next.t('dashboard.addAction.SUCCESS')
		const subAPIValue = userLevel.edit ? 'update' : 'add'

		const apiValue = await postServiceAPI(
			subAPIValue + 'userdata',
			{
				//body
				//blankObject key:value pairs
			},
			{
				//headers
			}
		)

		if (apiValue && Object.entries(apiValue).length > 0) {
			const infoData = apiValue.data
			const infoStatus = apiValue.statusCode
			if (infoData.hasOwnProperty('success')) {
			} else {
				showToast({
					open: true,
					variant: TOAST_VARIANT.ERROR,
					message: infoData.message
				})
			}
			setLoader(false)
		}
	}

	// delete method to user changes
	const deleteUserData = async item => {
		setLoader(true)
		const msg = i18next.t('dashboard.deleteAction.success')

		const apiValue = await deleteServiceAPI(
			'deleteuserdata',
			{
				//body
			},
			{
				//headers
			}
		)

		if (apiValue && Object.entries(apiValue).length > 0) {
			const infoData = apiValue.data
			const infoStatus = apiValue.statusCode
			if (infoData.hasOwnProperty('success')) {
			} else {
				showToast({
					open: true,
					variant: TOAST_VARIANT.ERROR,
					message: infoData.message
				})
			}
			setLoader(false)
		}
	}

	const registrationAsMember = type => {
		switch (type) {
			case 'new':
				let state = JSON.parse(JSON.stringify(helperValidation.dashboard))
				setHelper(state)
				setUserLevel({ ...userLevel, edit: false })
				setBlankObject({
					id: uuidv4(),
					firstname: '',
					lastname: '',
					emailid: '',
					phone: '',
					gender: 'Male',
					dob: dateFormatter(CurrentDate, 'YYYY-MM-DD'),
					active: true
				})
				setDialogOpen(true)
				break

			default:
				break
		}
	}

	const showOnCopy = id =>
		showToast({
			open: true,
			variant: TOAST_VARIANT.SUCCESS,
			message: `${'Copied User UID: "' + id + '" to clipboard'}`
		})

	const handleUserObj = (type, value) => {
		switch (type) {
			case 'first':
				setBlankObject({ ...blankObject, firstname: value })
				break
			case 'last':
				setBlankObject({ ...blankObject, lastname: value })
				break
			case 'contact':
				const avoidSpaceRegex = /^\S*$/
				if (avoidSpaceRegex.test(value))
					setBlankObject({ ...blankObject, phone: value })

				break
			case 'email':
				setBlankObject({ ...blankObject, emailid: value })
				break
			case 'dob':
				setOpenDatePicker(false)
				setBlankObject({
					...blankObject,
					dob: dateFormatter(value, 'YYYY-MM-DD')
				})
				break
			case 'gender':
				setBlankObject({ ...blankObject, gender: value })
				break
			case 'status':
				setBlankObject({ ...blankObject, active: value })
				break

			default:
				break
		}
	}

	const searchHandler = searchText => {
		setTableProperties({
			...tableProperties,
			...{ activePage: 0, searchText: searchText }
		})
	}

	const validateData = () => {
		let proceedToSave = true
		let state = JSON.parse(JSON.stringify(helperValidation.dashboard))

		if (blankObject.firstname && !customStrRegx.test(blankObject.firstname)) {
			proceedToSave = false
			state.firstname.isError = true
			state.firstname.text = i18next.t('dashboard.validations.NAME')
		}
		if (blankObject.lastname && !customStrRegx.test(blankObject.lastname)) {
			proceedToSave = false
			state.lastname.isError = true
			state.lastname.text = i18next.t('dashboard.validations.NAME')
		}
		if (blankObject.emailid && !emailRegex.test(blankObject.emailid)) {
			proceedToSave = false
			state.emailid.isError = true
			state.emailid.text = i18next.t('dashboard.validations.EMAIL')
		}
		if (blankObject.phone) {
			if (!customIntRegx.test(blankObject.phone)) {
				proceedToSave = false
				state.phone.isError = true
				state.phone.text = i18next.t('dashboard.validations.PHONE')
			} else if (
				blankObject.phone.length > 10 ||
				blankObject.phone.length < 10
			) {
				proceedToSave = false
				state.phone.isError = true
				state.phone.text = i18next.t('dashboard.validations.PHONELEN')
			}
		}
		if (!blankObject.firstname) {
			proceedToSave = false
			state.firstname.isError = true
			state.firstname.text = i18next.t('dashboard.validations.required.FIRST')
		}
		if (!blankObject.emailid) {
			proceedToSave = false
			state.emailid.isError = true
			state.emailid.text = i18next.t('dashboard.validations.required.EMAIL')
		}

		setHelper(state)
		proceedToSave ? updateMasterObject() : setLoader(false)
	}

	const updateMasterObject = () => {
		if (userLevel.edit) {
			const filterArray = masterObj.map(mo => {
				if (mo.id === blankObject.id) mo = blankObject
				return mo
			})
			setMasterObj(filterArray)
			showToast({
				open: true,
				variant: TOAST_VARIANT.SUCCESS,
				message: i18next.t('dashboard.editAction.SUCCESS')
			})
		} else {
			// debugger
			setMasterObj(currentArray => [...masterObj, blankObject])
			showToast({
				open: true,
				variant: TOAST_VARIANT.SUCCESS,
				message: i18next.t('dashboard.addAction.SUCCESS')
			})
		}
		setDialogOpen(false)
		setLoader(false)
	}

	const onSaveUser = () => {
		setLoader(true)
		userLevel.edit
			? console.log('Existing User Update', blankObject)
			: console.log('New User Addition', blankObject)
		validateData()
	}

	const onOkClick = () => {
		const newArr = masterObj.filter(mo => mo.id !== blankObject.id)
		setMasterObj(newArr)
		setBasicDialog({
			...basicDialog,
			show: false
		})
		showToast({
			open: true,
			variant: TOAST_VARIANT.WARNING,
			message: i18next.t('dashboard.deleteAction.success')
		})
	}

	const tableActionMethod = async e => {
		const data = e.row
		if (e.actionType === 'edit') {
			setUserLevel({ ...userLevel, edit: true })
			setBlankObject(e.row)
			setDialogOpen(true)
		} else if (e.actionType === 'delete') {
			setBasicDialog({
				...basicDialog,
				show: true,
				msg: `${i18next.t('dashboard.dialog.DELETE')} ${e.row.firstname} ${
					e.row.lastname
				} ?`,
				cancel: i18next.t('dashboard.CANCEL')
			})
			setBlankObject(e.row)
		}
	}

	//return jsx element
	return (
		<Grid container className='page-header__wrapper'>
			{loader && <Loader type='center-overlay' />}
			<Grid item xs className='dashboard__wrapper'>
				<main className='fi-container--full-height'>
					<Grid container className='registry-dashboard'>
						<Grid
							item
							xs={12}
							sm={12}
							md={12}
							className='registry-dashboard__header'>
							<Grid container className='registry-dashboard__heading'>
								<Grid item xs={12}>
									<Typography
										variant='h1'
										component='h2'
										className='registry-dashboard__title'>
										{i18next.t('dashboard.TITLE')}
									</Typography>
									<Typography
										variant='subtitle2'
										component='span'
										className='registry-dashboard__paragraph'>
										{i18next.t('dashboard.SUB_TITLE')}
									</Typography>
								</Grid>
							</Grid>
						</Grid>
						<Grid item xs={12} className='registry-dashboard__searchbar'>
							<Grid container>
								<Grid item xs={12}>
									<SearchBuilder
										searchControlLabel={i18next.t('dashboard.SEARCH_LABEL')}
										searchHandler={searchHandler}
										searchText={tableProperties.searchText}
										class='registry-dashboard__search-component'
									/>
									<Button
										variant='outlined'
										color='primary'
										onClick={() => registrationAsMember('new')}>
										{i18next.t('dashboard.ADD_USER')}
									</Button>
								</Grid>
								<Grid item xs={12} sm={12} md={12}>
									{tableData.length === 0 && (
										<NoResultFound message={i18next.t('dashboard.NO_UNIT')} />
									)}
									{tableData.length > 0 && (
										<TableBuilder
											tableData={tableData}
											columns={columns}
											actionMethod={tableActionMethod}
											tableDataCount={tableLength}
											pageList={pageList}
											tableProperties={tableProperties}
											setTableProperties={setTableProperties}
										/>
									)}
								</Grid>
							</Grid>
						</Grid>
						{dialogOpen && (
							<Dialog
								className='add-table-wrapper'
								open={true}
								maxWidth='sm'
								TransitionComponent={Transition}
								aria-labelledby='alert-dialog-title'
								aria-describedby='alert-dialog-description'>
								<DialogTitle
									id='alert-dialog-slide-title'
									className='popup__title'>
									<span className='title'>
										{userLevel.edit
											? i18next.t('dashboard.dialog.EDIT')
											: i18next.t('dashboard.dialog.ADD')}
									</span>
									<Typography
										variant='subtitle2'
										component='span'
										className='header__page-span'>
										<Tooltip
											placement='top-end'
											title={
												<Grid>
													<FontAwesomeIcon
														icon={['far', 'copy']}
														size='lg'
														className='edit-icon'></FontAwesomeIcon>
													{i18next.t('dashboard.dialog.tooltip.COPY')}
												</Grid>
											}>
											<CopyToClipboard
												text={blankObject.id}
												onCopy={() => showOnCopy(blankObject.id)}>
												<span style={{ cursor: 'pointer' }}>
													{blankObject.id}
												</span>
											</CopyToClipboard>
										</Tooltip>
									</Typography>
								</DialogTitle>
								<Grid className='instance-popup'>
									<Grid container className='add-instance-popup'>
										<Grid
											item
											xs={12}
											sm={4}
											md={4}
											className='add-instance-grp'>
											<TextField
												id='myInputFirst'
												className='add-instance-popup__textfield text-field-row'
												label={
													<div className='add-instance-popup__label'>
														{i18next.t('dashboard.dialog.FIRST')}
														<span className='mandatory-feild'>*</span>
													</div>
												}
												value={blankObject.firstname}
												onChange={e => handleUserObj('first', e.target.value)}
												error={helper.firstname.isError}
												helperText={helper.firstname.text}
											/>
										</Grid>
										<Grid
											item
											xs={12}
											sm={4}
											md={4}
											className='add-instance-grp'>
											<TextField
												id='myInputLast'
												className='add-instance-popup__textfield text-field-row'
												label={
													<div className='add-instance-popup__label'>
														{i18next.t('dashboard.dialog.LAST')}
													</div>
												}
												value={blankObject.lastname}
												onChange={e => handleUserObj('last', e.target.value)}
												error={helper.lastname.isError}
												helperText={helper.lastname.text}
											/>
										</Grid>
										<Grid
											item
											xs={12}
											sm={4}
											md={4}
											className='add-instance-grp calendar-field'>
											<MuiPickersUtilsProvider utils={DateFnsUtils}>
												<Grid item container>
													<KeyboardDatePicker
														disableToolbar
														disableFuture
														variant='inline'
														format='yyyy/MM/dd'
														id='date-picker-inline'
														open={openDatePicker}
														label={
															<div>
																{i18next.t('dashboard.dialog.DOB')}
																<span className='mandatory-feild'>*</span>
															</div>
														}
														value={blankObject.dob}
														onChange={e => handleUserObj('dob', e)}
														onClick={() => setOpenDatePicker(true)}
														KeyboardButtonProps={{
															'aria-label': 'change date'
														}}
														error={helper.dob.isError}
														fullWidth
													/>
												</Grid>
											</MuiPickersUtilsProvider>
											<FormHelperText error>{helper.dob.text}</FormHelperText>
										</Grid>
									</Grid>
									<Grid container className='add-instance-popup'>
										<Grid
											item
											xs={12}
											sm={4}
											md={4}
											className='add-instance-grp'>
											<TextField
												id='myInputMail'
												className='text-field-row'
												label={
													<div className='add-instance-popup__label'>
														{i18next.t('dashboard.dialog.EMAIL')}
														<span className='mandatory-feild'>*</span>
													</div>
												}
												value={blankObject.emailid}
												onChange={e => handleUserObj('email', e.target.value)}
												error={helper.emailid.isError}
												helperText={helper.emailid.text}
											/>
										</Grid>
										<Grid
											item
											xs={12}
											sm={4}
											md={4}
											className='add-instance-grp'>
											<TextField
												id='myInputPhone'
												className='text-field-row'
												label={
													<div className='add-instance-popup__label'>
														{i18next.t('dashboard.dialog.PHONE')}
													</div>
												}
												placeholder='+91'
												value={blankObject.phone}
												onChange={e => handleUserObj('contact', e.target.value)}
												error={helper.phone.isError}
												helperText={helper.phone.text}
											/>
										</Grid>
										<Grid
											item
											xs={12}
											sm={4}
											md={4}
											className='add-instance-grp user-status-switcher'>
											<FormControlLabel
												control={
													<Switch
														checked={blankObject.active}
														onChange={e =>
															handleUserObj('status', e.target.checked)
														}
														name='inactive'
														inputProps={{ 'aria-label': 'secondary checkbox' }}
													/>
												}
												label={`${i18next.t('dashboard.dialog.STATUS')} ${
													blankObject.active
														? i18next.t('dashboard.dialog.ACTIVE')
														: i18next.t('dashboard.dialog.INACTIVE')
												}`}
											/>
										</Grid>
									</Grid>
									<Grid container className='add-instance-popup gender'>
										<Grid
											item
											xs={12}
											sm={12}
											md={12}
											className='add-instance-grp '>
											<FormControl component='fieldset'>
												<FormLabel component='legend'>
													{i18next.t('dashboard.dialog.GENDER')}
												</FormLabel>
												<RadioGroup
													style={{ flexDirection: 'row' }}
													aria-label='gender'
													name='gender1'
													value={blankObject.gender}
													onChange={e =>
														handleUserObj('gender', e.target.value)
													}>
													<FormControlLabel
														value='Male'
														control={<Radio />}
														label='Male'
													/>
													<FormControlLabel
														value='Female'
														control={<Radio />}
														label='Female'
													/>
													<FormControlLabel
														value='Other'
														control={<Radio />}
														label='Other'
													/>
												</RadioGroup>
											</FormControl>
											<FormHelperText error={helper.gender.isError}>
												{helper.gender.text}
											</FormHelperText>
										</Grid>
									</Grid>

									<MessageSnackbar
										data={i18next.t('dashboard.dialog.NOTE')}
										type={'note'}
									/>

									<Grid container className='add-instance-popup__btns'>
										<Button
											variant='outlined'
											className='add-instance-popup__cancelBtn'
											onClick={() => setDialogOpen(false)}>
											{i18next.t('dashboard.CANCEL')}
										</Button>
										<Button
											variant='outlined'
											color='primary'
											className='add-instance-popup__saveBtn'
											onClick={onSaveUser}>
											{i18next.t('dashboard.SAVE')}
										</Button>
									</Grid>
								</Grid>
							</Dialog>
						)}
					</Grid>
				</main>
			</Grid>
			{basicDialog.show && (
				<BasicDialog
					isOpen={basicDialog.show}
					modalTitle={basicDialog.title}
					modalContent={basicDialog.msg}
					modalOkButtonText={'ok'}
					modalOkEvent={e => {
						onOkClick(e)
					}}
					modalCancelEvent={() =>
						setBasicDialog({ ...basicDialog, show: false })
					}
					modalCancelButtonText={basicDialog.cancel}
				/>
			)}
		</Grid>
	)
}

export default Dashboard
