import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import PropTypes from 'prop-types'
import {
	Grid,
	Table,
	TableHead,
	TableBody,
	TableRow,
	TableCell,
	TablePagination,
	TableSortLabel,
	Button,
	Paper
} from '@material-ui/core'
import TableContainer from '@material-ui/core/TableContainer'
import IconButton from '@material-ui/core/IconButton'
import FirstPageIcon from '@material-ui/icons/FirstPage'
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft'
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight'
import LastPageIcon from '@material-ui/icons/LastPage'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import NoResultFound from '../../components/NoResultFound'
import { makeStyles } from '@material-ui/styles'
import get from 'get-value'
import Moment from 'moment'
import { checkToDisable } from '../../utility/functions'

const defaultTableContext = {
	tableData: {},
	updateTableData: () => {}
}

const parseTableCellKeyValue = (data, field, isOnKey) => {
	const cellkey = field.mapProperty
		? isOnKey
			? field.showOnKey.split(',')
			: field.mapProperty.split(',')
		: {}
	if (cellkey.length > 1) {
		let returnValue = ''
		cellkey.forEach(element => {
			let elementData = get(data, element)
			if (elementData) returnValue += `${elementData} `
		})
		return field.dateColumn
			? Moment(returnValue).format(
					(field.dateFormat && field.dateFormat) || 'MM/DD/YYYY'
			  ) === 'Invalid date'
				? ''
				: Moment(returnValue).format(
						(field.dateFormat && field.dateFormat) || 'MM/DD/YYYY'
				  )
			: returnValue
	} else {
		let fieldName = isOnKey ? field.showOnKey : field.mapProperty
		const keyCellValue = get(data, fieldName)
		if (Array.isArray(keyCellValue)) {
			return keyCellValue.map(p => p[field.objectKeyToShow]).join(', ')
		} else
			return field.dateColumn
				? Moment(keyCellValue).format(
						(field.dateFormat && field.dateFormat) || 'MM/DD/YYYY'
				  ) === 'Invalid date'
					? ''
					: Moment(keyCellValue).format(
							(field.dateFormat && field.dateFormat) || 'MM/DD/YYYY'
					  )
				: typeof keyCellValue === 'boolean'
				? String(keyCellValue)
				: keyCellValue
	}
}

export const TableContext = React.createContext(defaultTableContext)

const useStyles = makeStyles({
	table: {
		minWidth: 650
	},
	strikeColor: {
		textDecoration: 'line-through'
	},
	TableHead: {
		backgroundColor: 'red'
	}
})

const actionsStyles = theme => ({
	root: {
		flexShrink: 0,
		color: theme.palette.text.secondary,
		marginLeft: theme.spacing(2.5)
	}
})

const TablePaginationActions = props => {
	const handleFirstPageButtonClick = event => {
		props.onChangePage(event, 0)
	}

	const handleBackButtonClick = event => {
		props.onChangePage(event, props.page - 1)
	}

	const handleNextButtonClick = event => {
		props.onChangePage(event, props.page + 1)
	}

	const handleLastPageButtonClick = event => {
		props.onChangePage(
			event,
			Math.max(0, Math.ceil(props.count / props.rowsPerPage) - 1)
		)
	}

	const { classes, count, page, rowsPerPage, theme } = props

	return (
		<div className={classes.root}>
			<IconButton
				onClick={handleFirstPageButtonClick}
				disabled={page === 0}
				aria-label='First Page'>
				{theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
			</IconButton>
			<IconButton
				onClick={handleBackButtonClick}
				disabled={page === 0}
				aria-label='Previous Page'>
				{theme.direction === 'rtl' ? (
					<KeyboardArrowRight />
				) : (
					<KeyboardArrowLeft />
				)}
			</IconButton>
			<IconButton
				onClick={handleNextButtonClick}
				disabled={page >= Math.ceil(count / rowsPerPage) - 1}
				aria-label='Next Page'>
				{theme.direction === 'rtl' ? (
					<KeyboardArrowLeft />
				) : (
					<KeyboardArrowRight />
				)}
			</IconButton>
			<IconButton
				onClick={handleLastPageButtonClick}
				disabled={page >= Math.ceil(count / rowsPerPage) - 1}
				aria-label='Last Page'>
				{theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
			</IconButton>
		</div>
	)
}

const TablePaginationActionsWrapped = withStyles(actionsStyles, {
	withTheme: true
})(TablePaginationActions)

const TableBuilder = props => {
	const classes = useStyles()

	const createSortHandler = property => event => {
		handleRequestSort(event, property)
	}
	const handleRequestSort = (event, property) => {
		const isAsc =
			props.tableProperties.orderBy === property.mapProperty &&
			props.tableProperties.order === 'asc'

		let tableProps = {
			order: isAsc ? 'desc' : 'asc',
			orderBy: property.mapProperty,
			objectKeyToShow: property.objectKeyToShow,
			activePage: 0
		}
		props.setTableProperties &&
			props.setTableProperties({
				...props.tableProperties,
				...tableProps
			})
	}
	const handleChangePage = (event, newPage) => {
		props.setTableProperties &&
			props.setTableProperties({
				...props.tableProperties,
				activePage: newPage
			})
	}
	const handleChangeRowsPerPage = event => {
		let tableProps = {
			activePage: 0,
			rowsPerPage: parseInt(event.target.value, 10)
		}
		props.setTableProperties &&
			props.setTableProperties({
				...props.tableProperties,
				...tableProps
			})
	}
	if (props.tableData.length === 0)
		return (
			<Grid item xs={12} className='table-root__wrapper'>
				<NoResultFound message={props.message || ''} />
			</Grid>
		)

	return (
		<Grid container className='table-root__wrapper'>
			<TableContainer component={Paper} className='table-container'>
				<Table aria-label='sticky table'>
					<TableHead>
						<TableRow>
							{props.columns.map((field, index) => (
								<TableCell
									key={'tablecell' + index}
									className={
										(field.className &&
											field.className.toLowerCase().replace(/\s/g, '')) ||
										(field.columnLabel &&
											field.columnLabel.toLowerCase().replace(/\s/g, ''))
									}>
									{field.isSort ? (
										<TableSortLabel
											active={
												props.tableProperties.orderBy === field.mapProperty
											}
											direction={
												props.tableProperties.orderBy === field.mapProperty
													? props.tableProperties.order
													: 'asc'
											}
											onClick={createSortHandler({
												mapProperty: field.mapProperty,
												objectKeyToShow:
													field.objectKeyToShow && field.objectKeyToShow
											})}>
											{field.columnLabel}
										</TableSortLabel>
									) : (
										field.columnLabel
									)}
								</TableCell>
							))}
						</TableRow>
					</TableHead>
					<TableBody>
						{props.tableData &&
							props.tableData.length > 0 &&
							//stableSort(props.tableData, getSorting(order, orderBy))
							//.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
							props.tableData.map((row, index) => (
								<TableRow
									key={row.id + '_' + index}
									className={
										props.strikeProps &&
										props.strikeProps.strikeOnColumn &&
										props.strikeProps.strikeOnValue ===
											get(row, props.strikeProps.strikeOnColumn) &&
										classes.strikeColor
									}>
									{props.columns.map((field, index) => (
										<TableCell
											key={'table' + index}
											className={
												(field.className &&
													field.className.toLowerCase().replace(/\s/g, '')) ||
												(field.columnLabel &&
													field.columnLabel.toLowerCase().replace(/\s/g, ''))
											}>
											{!field.columnActions
												? parseTableCellKeyValue(row, field, false)
												: field.columnActions.map((action, index) => {
														return (
															<ActionRenders
																key={index}
																{...action}
																row={row}
																mapProperty={field.mapProperty}
																actionMethod={props.actionMethod}
																currencyicon={
																	(props.currencyicon && props.currencyicon) ||
																	'dollar-sign'
																}
															/>
														)
												  })}
										</TableCell>
									))}
								</TableRow>
							))}
					</TableBody>
				</Table>
			</TableContainer>
			<Grid container spacing={2} className='table-pagination__wrapper'>
				<Grid item xs className='total-amount__container'>
					{props.totalAmountProps && (
						<TotalAmount
							{...props.totalAmountProps}
							currency={props.currencyicon}
						/>
					)}
				</Grid>
				<Grid item xs className='pagination__container'>
					{props.tableData &&
						props.tableData.length > 0 &&
						props.tableDataCount > Math.min.apply(Math, props.pageList) && (
							<TablePagination
								rowsPerPageOptions={props.pageList}
								component='div'
								count={props.tableDataCount}
								rowsPerPage={props.tableProperties.rowsPerPage}
								page={props.tableProperties.activePage}
								onChangePage={handleChangePage}
								onChangeRowsPerPage={handleChangeRowsPerPage}
								labelDisplayedRows={({ from, to, count }) =>
									`Records ${from}-${to} of ${count}`
								}
								SelectProps={{
									native: true
								}}
								ActionsComponent={TablePaginationActionsWrapped}
							/>
						)}
				</Grid>
			</Grid>

			{props.tableData && props.tableData.length === 0 && (
				<Grid item xs={12} className='table-root__wrapper'>
					No data found
				</Grid>
				// <div>No data found</div>
			)}
		</Grid>
	)
}

const ActionRenders = props => {
	let cellData = parseTableCellKeyValue(props.row, props, false)
	let applySpan = props.isApplySpan
		? props.isApplySpan === 'no'
			? false
			: true
		: true
	if (props.showOnKey) {
		let keyValue = parseTableCellKeyValue(props.row, props, true)
		if (props.showForValue.indexOf(keyValue && String(keyValue).trim()) === -1)
			return null
	}

	switch (props.buttonType) {
		case 'customIcon':
			let color = 'grey'
			if (props.dataColors) {
				let dataColorList =
					props.dataColors &&
					props.dataColors.length > 0 &&
					props.dataColors.find(e => {
						//if (props.row[props.mapProperty] !== null)
						if (cellData !== null)
							return (
								String(e.key).toLowerCase() === String(cellData).toLowerCase()
							)
						return null
					})
				color = dataColorList && dataColorList.value
			}

			if (!cellData) return null
			return (
				<span>
					<FontAwesomeIcon
						icon={['fas', props.icon || '']}
						className='action_icons'
						title={props.componentType}
						size='md'
						color={color}
						disabled='disabled'></FontAwesomeIcon>
				</span>
			)

		case 'icon':
			let iconIsDisable = false
			if (props.disableOnKey) {
				if (checkToDisable(props, props.row)) iconIsDisable = true
			}
			if (props.clientSideDisable) iconIsDisable = true

			if (iconIsDisable) {
				return (
					<>
						<span
							className={applySpan ? 'icon_btn__span disabled' : 'disabled'}
							title={
								props.disabledToolTipText
									? props.disabledToolTipText
									: props.componentType
							}>
							<FontAwesomeIcon
								icon={['far', props.icon]}
								className='action_icons disabled'
								size='md'
								color='grey'
								disabled='disabled'></FontAwesomeIcon>
						</span>
						{props.showCellData && cellData}
					</>
				)
			}

			return (
				<>
					<span
						className={applySpan ? 'icon_btn__span action' : ''}
						onClick={() => props.actionMethod && props.actionMethod(props)}
						title={props.componentType}>
						<FontAwesomeIcon
							icon={['far', props.icon]}
							// onClick={() =>
							//     props.actionMethod && props.actionMethod(props)
							// }
							className='action_icons'
							title={props.componentType}
							color={props.color && props.color}
							cursor='pointer'></FontAwesomeIcon>
					</span>
					{'   '}
					{props.showCellData && cellData}
				</>
			)

		case 'label':
			return (
				<span
					onClick={() => props.actionMethod && props.actionMethod(props)}
					className='fa fa-fw'>
					{props.componentType}
				</span>
			)

		case 'dataLabelWithAction':
			return (
				<span
					onClick={() => props.actionMethod && props.actionMethod(props)}
					className='link-txt__label'
					style={{
						color: props.color ? props.color : 'blue',
						cursor: 'pointer'
					}}>
					{parseTableCellKeyValue(props.row, props)}
					{/* {props.row[props.mapProperty]} */}
				</span>
			)

		case 'dataLabel':
			// let dataColor
			// let dataColorList =
			//     props.dataColors &&
			//     props.dataColors.length > 0 &&
			//     props.dataColors.find(e => {
			//         //if (props.row[props.mapProperty] !== null)
			//         if (cellData !== null)
			//             return (
			//                 String(e.key).toLowerCase() === String(cellData).toLowerCase()
			//             )
			//         return null
			//     })
			// dataColor = dataColorList && dataColorList.value
			let cellChangeList =
				props.dataChange &&
				props.dataChange.length > 0 &&
				props.dataChange.find(e => {
					// if (props.row[props] !== null)
					// if (cellData !== null)
					if (e.key && cellData)
						return (
							String(e.key).toLowerCase() === String(cellData).toLowerCase()
						)

					return null
				})
			let cData =
				props.dataChange && props.dataChange.length > 0 && cellChangeList
					? cellChangeList.value
					: cellData

			if (!cData) return null
			return (
				<span
					className={
						cData &&
						'status-chip ' +
							String(cData)
								.toLowerCase()
								.replace(/\s/g, '')
					}
					// style={{color: dataColor}}
				>
					{cData}
				</span>
			)

		case 'button':
			return (
				<Button
					variant='contained'
					color='primary'
					onClick={() => props.actionMethod && props.actionMethod(props)}
					className='fa fa-fw'>
					{props.componentType}
				</Button>
			)

		case 'outline-button':
			return (
				<Button
					variant='outlined'
					color='primary'
					onClick={() => props.actionMethod && props.actionMethod(props)}
					className='fa fa-fw'>
					{props.componentType}
				</Button>
			)

		case 'labelButtonAction':
			// Case written for the handle TIN on the Clinician Grid.
			return (
				<div>
					{(props.row.tinlist.length !== 0 && props.row.tinlist.length !== 1) ||
					props.row.tinlist.length === 1 ? (
						<Button
							variant='outlined'
							color='primary'
							onClick={() => props.actionMethod && props.actionMethod(props)}>
							{props.row.tinlist.length === 1
								? props.row.tinlist
								: props.row.tinlist[0]
										.toString()
										.concat(' (+', props.row.tinlist.length - 1, ')')}
						</Button>
					) : (
						<Button
							variant='outlined'
							color='primary'
							onClick={() => props.actionMethod && props.actionMethod(props)}>
							+ {props.buttonLabel}
						</Button>
					)}
				</div>
			)
		default:
			return null
	}
}

const TotalAmount = props => {
	if (props.amount === '') return null
	return (
		<Grid item xs className='amount-wrapper'>
			<span>{props.labelText && props.labelText}</span>
			<FontAwesomeIcon
				icon={['far', props.currency]}
				className='currency-sign'
				size='md'></FontAwesomeIcon>
			<span className='amount'>{props.amount && props.amount}</span>
			<span className='incldingText'>
				{props.includingText && props.includingText}
			</span>
		</Grid>
	)
}

TableBuilder.propTypes = {
	tableData: PropTypes.array,
	columns: PropTypes.array,
	tableDataCount: PropTypes.number,
	pageList: PropTypes.array,
	tableProperties: PropTypes.object,
	setTableProperties: PropTypes.func
}

TableBuilder.defaultProps = {
	pageList: [5, 10, 20, 50, 100],
	tableProperties: { order: 'asc', orderBy: '', rowsPerPage: 5, activePage: 0 }
}
export default TableBuilder
