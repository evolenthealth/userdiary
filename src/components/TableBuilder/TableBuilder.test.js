
import React from 'react'
import { shallow } from 'enzyme'
import TableBuilder from './TableBuilder'

describe('<Footer />', () => {
	test('renders', () => {
		const wrapper = shallow(<TableBuilder />)
		expect(wrapper).toMatchSnapshot()
	})
})
