import React from 'react'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import { Button } from '@material-ui/core'
import ReactHtmlParser from 'react-html-parser'
import i18next from 'i18next'

const BasicDialog = props => {
	return (
		<Dialog
			className='basic-dialog'
			open={props.isOpen}
			maxWidth='sm'
			onClose={props.handleCloseEvent}
			aria-labelledby='alert-dialog-title'
			aria-describedby='alert-dialog-description'>
			{props.modalTitle && (
				<DialogTitle className='basic-dialog__title' id='alert-dialog-title'>
					{props.modalTitle}
				</DialogTitle>
			)}
			{props.modalContent && (
				<DialogContent className='basic-dialog__content'>
					<DialogContentText id='alert-dialog-description'>
						{props.modalContent}
					</DialogContentText>
				</DialogContent>
			)}
			{props.parserModalContent && (
				<DialogContent className='basic-dialog__content'>
					<DialogContentText>
						{ReactHtmlParser(props.parserModalContent)}
					</DialogContentText>
				</DialogContent>
			)}

			<DialogActions className='basic-dialog__actions'>
				{props.modalCancelButtonText && (
					<Button
						variant='outlined'
						className='basic-dialog__cancelBtn'
						onClick={props.modalCancelEvent}
						//  className='primary-btn' color='primary'
					>
						{props.modalCancelButtonText}
					</Button>
				)}
				<Button
					variant='outlined'
					color='primary'
					className='basic-dialog__okBtn'
					onClick={props.modalOkEvent}
					// className='primary-btn'
					// color='primary'
					//autoFocus
				>
					{props.modalOkButtonText}
				</Button>
			</DialogActions>
		</Dialog>
	)
}

export default BasicDialog
