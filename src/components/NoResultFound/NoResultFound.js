import React from 'react'
import { Grid } from '@material-ui/core'
import NoResult from '../../assets/SVG/NoResult.svg'

const NoResultFound = props => {
	let msg = props.message ? props.message : 'No Data Found'
	const imagePath = 'No_result_found.svg'

	return (
		<Grid container className={`${props.class} no--result_wrapper`}>
			<Grid item className='no--result_grid'>
				<Grid item className='no--result_content'>
					<h2 className='no--result_heading'>{msg}</h2>
				</Grid>
				<Grid item className='no--result_img-grid'>
					<img
						src={NoResult}
						alt='No Result Found'
						className='page-not-found_img'
					/>
				</Grid>
			</Grid>
		</Grid>
	)
}

export default NoResultFound
