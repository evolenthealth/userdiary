import React from 'react'
import {CircularProgress, LinearProgress} from '@material-ui/core'
import PropTypes from 'prop-types'

const Loader = props => {
    if (props.loading) {
        if (props.type === 'page') {
            return (
                <LinearProgress
                    style={{}}
                    className="loader__linear-progress"
                    color={props.color}
                />
            )
        }

        if (props.type === 'center-overlay') {
            return (
                <div
                    className="loader__circular-overlay"
                    style={{
                        left: 0,
                        top: 0,
                        width: '100%',
                        height: '100%',
                        position: 'fixed',
                        zIndex: '10000',
                        backgroundColor: 'rgba(0, 0, 0, 0.5)',
                    }}
                >
                    <div
                        className="loader__circular-overlay__inner"
                        style={{
                            left: 0,
                            top: 0,
                            width: '100%',
                            height: '100%',
                            position: 'absolute',
                        }}
                    >
                        <CircularProgress
                            style={{
                                position: 'absolute',
                                left: '50%',
                                top: '35%',
                                zIndex: '1000',
                            }}
                            className="loader__circular-progress"
                            color={props.color}
                        />
                    </div>
                </div>
            )
        }

        return (
            <CircularProgress className="loader__circular-progress" color={props.color} />
        )
    }
    return null
}

Loader.propTypes = {
    type: PropTypes.string,
    loading: PropTypes.bool,
    color: PropTypes.string,
}

Loader.defaultProps = {
    type: 'circular',
    color: 'primary',
    loading: true,
}

export default Loader
