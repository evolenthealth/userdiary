import React from 'react'
import { Grid } from '@material-ui/core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import MarkdownRender from '../MarkdownRender'

const MessageSnackbar = props => {
	let toShow = props.mandatory === undefined ? true : props.mandatory
	const RenderHeader = () => {
		switch (props.type.toLowerCase()) {
			case 'note':
				return (
					<Grid className='note' container>
						<FontAwesomeIcon
							icon={['far', 'sticky-note']}
							className='icon-sticky-note'></FontAwesomeIcon>
						Note
					</Grid>
				)

			case 'info':
				return (
					<Grid className='info' container>
						<FontAwesomeIcon
							icon={['fal', 'info-circle']}
							className='fa fa-fw info-icon'></FontAwesomeIcon>
						Info
					</Grid>
				)

			default:
				break
		}
	}
	return (
		<Grid container className={`${props.type.toLowerCase()}-message`}>
			<RenderHeader />
			{toShow && (
				<Grid className='add-instance-popup__note note--main'>
					<span className='mandatory-feild'>* </span> Indicates mandatory
					fields.
				</Grid>
			)}
			<Grid className='add-instance-popup__note note--sub'>
				<MarkdownRender value={props.data}></MarkdownRender>
			</Grid>
		</Grid>
	)
}

export default MessageSnackbar
