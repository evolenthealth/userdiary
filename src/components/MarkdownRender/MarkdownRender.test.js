import React from 'react';
import { shallow } from 'enzyme';
import MarkdownRender from './MarkdownRender';

describe('<Footer />', () => {
  test('renders', () => {
    const wrapper = shallow(<MarkdownRender />);
    expect(wrapper).toMatchSnapshot();
  });
});
