import React from 'react'
import PropTypes from 'prop-types'
import ReactMarkdown from 'react-markdown/with-html'
import {Grid} from '@material-ui/core'
const MarkdownRender = props => {
    // const markdown =
    //     'Note: This is a sample note\n\n1. This could be paragraph\n2. Or just a sentence\n3. Or some <script>alert("Hello! I am an alert box!")</script>'

    //Do note, for security concerns we have checked and while this markdown rendered can render html, it should not be able to render script tags.
    // If it can, or if you would like to increase security, please read here: https://github.com/rexxars/react-markdown#parsing-html

    return (
        <Grid item xs className={`section-note__container ${props.cssClass}`}>
            <ReactMarkdown source={props.value} escapeHtml={false} />
        </Grid>
    )
}

MarkdownRender.defaultProps = {
    value: '',
}

MarkdownRender.propTypes = {
    value: PropTypes.string.isRequired,
}

export default MarkdownRender
