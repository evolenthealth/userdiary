import React, {useEffect, useContext} from 'react'
import MuiAlert from '@material-ui/lab/Alert'
import Snackbar from '@material-ui/core/Snackbar'
import {ToastContext} from '../../App'
import MarkdownRender from '../MarkdownRender'

const Toast = props => {
    const {showToast} = useContext(ToastContext)

    const [state, setState] = React.useState({
        open: props.open || false,
        vertical: props.vertical || 'top',
        horizontal: props.horizontal || 'center',
        autoHideDuration: props.autoHideDuration || 5000,
        variant: '',
    })

    useEffect(() => {
        setState({...state, ...props})
    }, [props])

    const {open, vertical, horizontal, autoHideDuration, variant} = state
    const handleClose = () => {
        if (props.onFocusOut) {
            setTimeout(() => {
                clearData()
            }, autoHideDuration)
        } else {
            clearData()
        }
    }

    const clearData = () => {
        showToast({
            open: false,
            variant: '',
            message: '',
            isHTML: false,
        })
        setState({
            open: false,
            vertical: 'top',
            horizontal: 'center',
            autoHideDuration: 5000,
            variant: '',
        })
    }
    return (
        <Snackbar
            anchorOrigin={{vertical, horizontal}}
            key={`${vertical},${horizontal}`}
            autoHideDuration={autoHideDuration}
            open={open}
            onClose={handleClose}
        >
            {props.message && (
                <Alert onClose={handleClose} severity={variant}>
                    {(props.isHTML && (
                        <MarkdownRender
                            value={
                                '<span style="color:white">' + props.message + '</span>'
                            }
                        ></MarkdownRender>
                    )) ||
                        props.message}
                </Alert>
            )}
        </Snackbar>
    )
}

const Alert = props => <MuiAlert elevation={6} variant="filled" {...props} />

export default Toast
