import React from 'react';
import { shallow } from 'enzyme';
import SelectIcon from './SelectIcon';

describe('<SelectIcon />', () => {
  test('renders', () => {
    const wrapper = shallow(<SelectIcon />);
    expect(wrapper).toMatchSnapshot();
  });
});
