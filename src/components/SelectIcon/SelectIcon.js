import React from 'react'
import InfoOutlinedIcon from '@material-ui/icons/InfoOutlined'

const SelectIcon = props => {
	let IconComp = () => <div></div>
	if (props.icon.iconType) {
		switch (props.icon.iconType) {
			case 'InfoOutlinedIcon':
				IconComp = InfoOutlinedIcon
				break

			default:
				// console.log('No icon display')
				break
		}
	}
	return <IconComp />
}

export default SelectIcon
