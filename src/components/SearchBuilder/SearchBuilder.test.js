import React from 'react'
import { shallow } from 'enzyme'
import SearchClientSide from './SearchClientSide'

describe('<Footer />', () => {
	test('renders', () => {
		const wrapper = shallow(<SearchClientSide />)
		expect(wrapper).toMatchSnapshot()
	})
})
