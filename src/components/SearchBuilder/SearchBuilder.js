import React, { useState, useEffect } from 'react'
import SearchIcon from '@material-ui/icons/Search'
import { IconButton, TextField, Grid, Input } from '@material-ui/core'
import PropTypes from 'prop-types'
import InputAdornment from '@material-ui/core/InputAdornment'

const SearchClientSide = props => {
	const [searchData, setSearchData] = useState(null)

	useEffect(() => {
		setSearchData(props.searchText)
	}, [props.searchText])

	const onChangeSearch = e => {
		setSearchData(e.target.value)
		search(e.target.value)
	}

	const search = async sData => {
		props.searchHandler && props.searchHandler(sData)
	}
	const handleSearchBtn = async sData => {
		// search(searchData)
	}

	const onChangeHelper = e => {
		const tvalue = e.target.value || ''
		if (tvalue && onValidateRegex(tvalue)) onChangeSearch(e)
		else if (tvalue === '') onChangeSearch(e)
	}
	const onValidateRegex = code => {
		let val = true
		switch (props.searchMatchInputType) {
			case 'integerOnly':
				const customIntRegx = /^[0-9\b]+$/
				if (!customIntRegx.test(code)) val = false
				break

			case 'stringOnly':
				const customStrRegx = /^[A-Za-z'.-\s]+$/
				if (!customStrRegx.test(code)) val = false
				break

			case 'alphaNumeric':
				const customalphaNumRegx = /^[A-Za-z\d\s]*$/
				if (!customalphaNumRegx.test(code)) val = false
				break

			case 'alphaSpecial':
				const customalphaSpRegx = /^[a-zA-Z!”@#$%&’()*+,/;[\\\]^_`{|}~\s]+$/
				if (!customalphaSpRegx.test(code)) val = false
				break

			default:
				break
		}
		return val
	}

	return (
		<Grid item xs className='search-console__div'>
			<TextField
				className={props.class || ''}
				value={searchData || ''}
				onChange={onChangeHelper}
				onKeyDown={props.onKeyDown || null}
				id={`standard-search-${props.searchControlLabel}`}
				// label={props.searchControlLabel}
				type='search'
				disabled={props.disableSearch || false}
				fullWidth
				placeholder={props.searchControlLabel}
				// InputLabelProps={
				// 	{
				// 		shrink: true,
				// 	}
				// }
				autoComplete='off'
				// onKeyPress={handleOnKeyPress}
				InputProps={{
					endAdornment: (
						<InputAdornment position='end'>
							<IconButton
								type='button'
								aria-label='search'
								color='primary'
								onClick={() => handleSearchBtn()}>
								<SearchIcon />
							</IconButton>
						</InputAdornment>
					)
				}}
			/>
		</Grid>
	)
}

SearchClientSide.propTypes = {
	searchControlLabel: PropTypes.string.isRequired,
	searchHandler: PropTypes.func.isRequired
}
export default SearchClientSide
