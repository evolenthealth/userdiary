import axios from 'axios'
import i18next from 'i18next'
import { UtilityConfig } from '../utility/constants'

//With PUT and POST requests, the 2nd argument is the request body, so you should pass an object with ,
//a headers property as the 3rd argument.
//: Used to add or update a record(s)
export const postServiceAPI = async (method, request, headers) => {
	return await axios
		.post(UtilityConfig.gatewayURL + method, request, {
			headers: headers
		})
		.then(success => {
			return generateResponse(success.status, success.data)
		})
		.catch(error => {
			return generateResponse(error.response.status, {
				error: true,
				response: error.response.data,
				message:
					error.response.status === 401
						? `${i18next.t('generic_msg.SESSION_EXP') +
								' with API ' +
								method +
								' for type POST.'}`
						: error.response.status !== 200
						? i18next.t('generic_msg.API_FAIL')
						: ''
			})
		})
}

//With GET request, you should pass an object with a headers property as the 2nd argument.
//: Used to fetch single or multiple records
export const getServiceAPI = async (method, headers) => {
	return await axios
		.get(UtilityConfig.gatewayURL + method, {
			headers: headers
		})
		.then(success => {
			return generateResponse(success.status, success.data)
		})
		.catch(error => {
			return generateResponse(error.response.status, {
				error: true,
				response: error.response.data,
				message:
					error.response.status === 401
						? `${i18next.t('generic_msg.SESSION_EXP') +
								' with API ' +
								method +
								' for type GET.'}`
						: error.response.status !== 200
						? i18next.t('generic_msg.API_FAIL')
						: ''
			})
		})
}

//With DELETE request, you should use the data option as the 2nd param
//: Used to delete single or multiple records
export const deleteServiceAPI = async (method, data, headers) => {
	return await axios
		.delete(UtilityConfig.gatewayURL + method, {
			data: data,
			headers: headers
		})
		.then(success => {
			return generateResponse(success.status, success.data)
		})
		.catch(error => {
			return generateResponse(error.response.status, {
				error: true,
				response: error.response.data,
				message:
					error.response.status === 401
						? `${i18next.t('generic_msg.SESSION_EXP') +
								' with API ' +
								method +
								' for type DELETE.'}`
						: error.response.status !== 200
						? i18next.t('generic_msg.API_FAIL')
						: ''
			})
		})
}

const generateResponse = (code, dataValue) => {
	return { statusCode: code, data: dataValue }
}
