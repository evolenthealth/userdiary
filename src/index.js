import React from 'react'
import { render } from 'react-dom'
import './index.css'
import App from './App'
import faIcons from './fontAwesome'
import { library } from '@fortawesome/fontawesome-svg-core'
import * as serviceWorker from './serviceWorker'
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles'
import { BrowserRouter } from 'react-router-dom'
import i18n from './i18n'
const theme = createMuiTheme({
	palette: {
		primary: { main: '#4194F2' }, // Purple and green play nicely together.
		secondary: { main: '#6ABC6A', contrastText: '#fff' }, // This is just green.A700 as hex.
		error: { main: '#F44336' }
	}
})

let basename = '/'

library.add(faIcons)

// ReactDOM.render(
// 	<React.StrictMode>
// 		<MuiThemeProvider theme={theme}>
// 			<App />
// 		</MuiThemeProvider>
// 	</React.StrictMode>,
// 	document.getElementById('root'),
// )

i18n.on('initialized', function(options) {
	render(
		<BrowserRouter basename={basename}>
			<MuiThemeProvider theme={theme}>
				<App />
			</MuiThemeProvider>
		</BrowserRouter>,
		document.getElementById('root')
	)
})

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
