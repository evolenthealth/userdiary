import {
	faCopy,
	faEdit,
	faTrashAlt,
	faStickyNote
} from '@fortawesome/free-regular-svg-icons'

export default [faCopy, faEdit, faTrashAlt, faStickyNote]
