import React, {
	createContext,
	Suspense,
	useContext,
	useEffect,
	useState
} from 'react'
import {
	BrowserRouter as Router,
	Route,
	Switch,
	useHistory
} from 'react-router-dom'
import './App.css'
import { Grid } from '@material-ui/core'
import Dashboard from './container/Dashboard'
import Loader from './components/Loader'
import Toast from './components/Toast'
import { SettingsProvider, useSettings } from './utility/customSettings'
import { UtilityConfig, TOAST_VARIANT } from './utility/constants'

export const ToastContext = createContext()

const ToastComp = props => {
	const [toastInfo, setToastInfo] = useState({})
	return (
		<ToastContext.Provider value={{ toastInfo, showToast: setToastInfo }}>
			<Toast {...toastInfo} />
			{props.children}
		</ToastContext.Provider>
	)
}

const App = () => {
	return (
		<Router>
			<ToastComp>
				<SettingsProvider>
					<Grid>
						<Switch>
							<Suspense fallback={<Loader type='page' />}>
								{/* setting default route to dashboard container */}
								<Route exact path='/' component={Dashboard}></Route>
							</Suspense>
						</Switch>
					</Grid>
				</SettingsProvider>
			</ToastComp>
		</Router>
	)
}

export default App
