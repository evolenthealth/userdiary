export const tableSchema = [
	{
		mapProperty: 'firstname',
		columnLabel: 'First Name',
		isSort: true,
		isFilter: true
	},
	{
		mapProperty: 'lastname',
		columnLabel: 'Last Name',
		isSort: true,
		isFilter: true
	},
	{
		mapProperty: 'emailid',
		columnLabel: 'Email',
		isSort: true,
		isFilter: true
	},
	{
		mapProperty: 'phone',
		columnLabel: 'Phone',
		isSort: true,
		isFilter: true
	},
	{
		mapProperty: 'gender',
		columnLabel: 'Gender',
		isSort: true,
		isFilter: true
	},
	{
		mapProperty: 'dob',
		columnLabel: 'Date Of Birth',
		isSort: true,
		isFilter: true
	},
	{
		mapProperty: 'active',
		columnLabel: 'Is Active',
		isSort: true,
		isFilter: true
	},
	{
		mapProperty: 'actions',
		columnLabel: 'Actions',
		actions: true,
		columnActions: [
			{
				componentType: 'Edit',
				actionType: 'edit',
				buttonType: 'icon',
				icon: 'edit'
			},
			{
				componentType: 'Delete',
				actionType: 'delete',
				buttonType: 'icon',
				icon: 'trash-alt'
			}
		]
	}
]

export const pageList = [5, 10, 15, 20]

export const UtilityConfig = {
	gatewayURL: process.env.REACT_APP_API_GATEWAY_URL,
	uiVersion: process.env.REACT_APP_VERSION
}

export const CurrentDate = new Date()

export const TOAST_VARIANT = {
	SUCCESS: 'success',
	ERROR: 'error',
	WARNING: 'warning'
}

export const helperValidation = {
	dashboard: {
		firstname: { isError: false, text: '' },
		lastname: { isError: false, text: '' },
		emailid: { isError: false, text: '' },
		phone: {
			isError: false,
			text: ''
		},
		dob: { isError: false, text: '' },
		gender: { isError: false, text: '' }
	}
}

//stringonly
export const customStrRegx = /^[A-Za-z'.-\s]+$/
//integer only regex
export const customIntRegx = /^[0-9\b]+$/
//email regex
export const emailRegex = /^[a-zA-Z0-9]+([\.\-\_][a-zA-Z0-9]+)*@[a-zA-Z0-9]+([\-\.][a-zA-Z0-9]+)*(\.[a-zA-Z]{2,4})$/
