import React, { useState, useEffect, useContext } from 'react'
import i18next from 'i18next'
import { ToastContext } from '../App'
import Loader from '../components/Loader'
import { TOAST_VARIANT, UtilityConfig } from '../utility/constants'
import { useHistory } from 'react-router'

export const SettingContext = React.createContext()

export const SettingsProvider = props => {
	const [loading, setLoading] = useState(false)
	const [masterObj, setMasterObj] = useState([])
	const history = useHistory()
	const { showToast } = useContext(ToastContext)

	if (loading) {
		return (
			<div
				style={{
					marginLeft: '48%',
					marginTop: '25%'
				}}>
				<Loader />
			</div>
		)
	}

	return (
		<div className={'reg-page__wrapper'}>
			<SettingContext.Provider
				value={{
					masterObj,
					setMasterObj
				}}>
				{props.children}
			</SettingContext.Provider>
		</div>
	)
}

export const useSettings = () => {
	return useContext(SettingContext)
}
