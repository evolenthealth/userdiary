import get from 'get-value'
import moment from 'moment'

const getColumsData = (data, filterKeys) => {
	const cellkey = filterKeys.mapProperty.split(',')
	if (cellkey.length > 1) {
		let returnValue = ''
		cellkey.forEach(element => {
			let elementData = get(data, element)
			if (elementData) returnValue += `${elementData} `
		})
		return returnValue === null ? '' : returnValue
	} else {
		const keyCellValue = get(data, filterKeys.mapProperty)
		if (Array.isArray(keyCellValue)) {
			return keyCellValue.map(p => p[filterKeys.objectKeyToShow]).join(', ')
		} else return keyCellValue === null ? '' : keyCellValue
	}
}

const nestedFilter = (targetArray, filterKeys, searchData) => {
	let filterData = []
	filterKeys.map(eachKey => {
		let fData = targetArray.filter(eachObj => {
			let getValue = getColumsData(eachObj, eachKey)
			return getValue
				? String(getValue)
						.toLowerCase()
						.indexOf(searchData) !== -1
				: false
		})
		if (fData.length > 0) {
			fData.map(d => {
				filterData.push(d)
			})
			targetArray = targetArray.filter(eachObj => {
				let getValue = getColumsData(eachObj, eachKey)

				return getValue
					? String(getValue)
							.toLowerCase()
							.indexOf(searchData) === -1
					: false
			})
		}
	})
	return filterData
}

const tableSort = (array, cmp) => {
	const stabilizedThis = array.map((el, index) => [el, index])
	stabilizedThis.sort((a, b) => {
		const order = cmp(a[0], b[0])
		if (order !== 0) return order
		return a[1] - b[1]
	})
	return stabilizedThis.map(el => el[0])
}

const desc = (a, b, tableProps) => {
	let filterKeys = {}
	filterKeys.mapProperty = tableProps.orderBy
	filterKeys.objectKeyToShow = tableProps.objectKeyToShow
	let aOrderBy = getColumsData(a, filterKeys)
	let bOrderBy = getColumsData(b, filterKeys)

	if (bOrderBy < aOrderBy) return -1
	if (bOrderBy > aOrderBy) return 1
	return 0
}

const getSorting = tableProps => {
	return tableProps.order === 'desc'
		? (a, b) => desc(a, b, tableProps)
		: (a, b) => -desc(a, b, tableProps)
}

export const searchOnData = (searchText, columns, data) => {
	if (searchText && searchText.trim() !== '') {
		let colList = columns.filter(e => {
			return e.isFilter
		})
		// let filterKeys = colList.map(e => {
		//     return e.mapProperty
		// })
		return nestedFilter(data, colList, searchText.toLowerCase())
	} else return data
}

export const getFilterSortedData = (data, tableProperties) => {
	try {
		if (data.length > 0) {
			let sorting = getSorting(tableProperties)
			let sData = tableSort(data, sorting).slice(
				tableProperties.activePage * tableProperties.rowsPerPage,
				tableProperties.activePage * tableProperties.rowsPerPage +
					tableProperties.rowsPerPage
			)

			if (sData.length === 0 && tableProperties.activePage > 0) {
				tableProperties.activePage = tableProperties.activePage - 1
				sData = tableSort(data, sorting).slice(
					tableProperties.activePage * tableProperties.rowsPerPage,
					tableProperties.activePage * tableProperties.rowsPerPage +
						tableProperties.rowsPerPage
				)
				return sData
			} else return sData
		} else return []
	} catch (e) {
		return []
	}
}

export const checkToDisable = (props, formData) => {
	if (props.disableOnKey) {
		let disableOnKeyValue = get(formData, props.disableOnKey)
		if (props.disableForCondition) {
			switch (props.disableForCondition) {
				case 'is-empty':
					if (!disableOnKeyValue) return true
					else return false
				case 'not-empty':
					if (disableOnKeyValue) return true
					else return false
				case 'not-equal':
					// if (!props.disableForConditionValue || !disableOnKeyValue)
					//     return false

					if (
						String(disableOnKeyValue) !== String(props.disableForConditionValue)
					)
						return true
					else return false
				default:
					return true
			}
		} else if (props.disableForValue) {
			if (
				String(disableOnKeyValue) === String(props.disableForValue) ||
				String(disableOnKeyValue) === 'null'
			)
				return true
			else return false
		} else {
			return false
		}
	} else return true
}

export const dateFormatter = (date, style) => {
	return date ? moment(date).format(style) : null
}
