## Author

Digvijayrao Jadhav (91+ 9921191241)

## Created Date

11 March 2021

## Modified Date

18 March 2021

## Organization

Test Project created for Evolent Health

## UserDiary : Project Name

Manage basic user profile including firstname, lastname, phonenumber, date of birth, gender, emailid & status.
i18n translation library is also added for localization
Note:
a. The application is created with npx-create-react-app @ReactVersion: v17.0.1 & @NodeVersion: v14.16.0
b. Here global-local object is maintaining the state, if the page is refreshed the object will be set to null

## Folder Structure

# assets

SVG
CSS

# components

Includes all the supporting individual functional components

# container

Includes the dashboard container that renders the components

# fontAwesome

Includes fab.js, fal.js, far.js, fas.js used for importing fontawesome icons

# services

Includes apiEndpoint.js that consists of axios http methods like PUT,GET,DELETE

# utility

Consists of constants, customSettings & functions js file for creating more granular functions.

# Steps to Run the Application

In Terminal: 1) npm install 2) npm run start
